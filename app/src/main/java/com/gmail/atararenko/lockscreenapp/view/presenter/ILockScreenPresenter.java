package com.gmail.atararenko.lockscreenapp.view.presenter;

public interface ILockScreenPresenter {

    void validatePassphrase();

}
