package com.gmail.atararenko.lockscreenapp.view;

import android.graphics.drawable.Drawable;

public interface ILockScreenView {

    void setWallpaper(Drawable image);

    void lockScreen();

    void unlockScreen();

    void unlockScreenAndFinish();

    String getEnteredPass();

    void setGreetingText(String greetingText);

}
