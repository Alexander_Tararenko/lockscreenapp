package com.gmail.atararenko.lockscreenapp.view.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.atararenko.lockscreenapp.R;
import com.gmail.atararenko.lockscreenapp.view.ILockScreenView;
import com.gmail.atararenko.lockscreenapp.view.presenter.ILockScreenPresenter;
import com.gmail.atararenko.lockscreenapp.view.presenter.LockScreenPresenter;
import com.gmail.atararenko.lockscreenapp.view.utils.LockScreenService;
import com.gmail.atararenko.lockscreenapp.view.utils.LockScreenUtils;

public class LockScreenActivity extends AppCompatActivity implements ILockScreenView {

    private ImageView mImageBackground;
    private TextView mGreetingsText;
    private TextInputEditText mEditTextPassphrase;
    private ILockScreenPresenter mPresenter;
    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mPresenter != null) {
                mPresenter.validatePassphrase();
            }
        }
    };
    private LockScreenUtils mLockScreenUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mImageBackground = (ImageView) findViewById(R.id.image_background);
        this.mEditTextPassphrase = (TextInputEditText) findViewById(R.id.passphrase);
        this.mGreetingsText = (TextView) findViewById(R.id.greetings_text);
        mLockScreenUtils = new LockScreenUtils();
        mPresenter = new LockScreenPresenter(this, this);
        mEditTextPassphrase.addTextChangedListener(mTextWatcher);
        startService(new Intent(this, LockScreenService.class));
        StateListener phoneStateListener = new StateListener();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void setWallpaper(Drawable image) {
        this.mImageBackground.setImageDrawable(image);
    }

    @Override
    public void lockScreen() {
        mLockScreenUtils.lock(this);
        //hideNavigation();
    }

    @Override
    public void unlockScreen() {
        mLockScreenUtils.unlock();
    }

    @Override
    public void unlockScreenAndFinish() {
        mLockScreenUtils.unlock();
        finish();
    }

    @Override
    public String getEnteredPass() {
        return mEditTextPassphrase != null ? mEditTextPassphrase.getText().toString() : "";
    }

    @Override
    public void setGreetingText(String greetingText) {
        if (greetingText != null) {
            mGreetingsText.setText(greetingText);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        lockScreen();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // disable recent tasks button
        ActivityManager activityManager = (ActivityManager)
                this.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.moveTaskToFront(getTaskId(), 0);
    }

    @Override
    public void onBackPressed() {
        hideSoftKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unlockScreen();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
                || (keyCode == KeyEvent.KEYCODE_POWER)
                || (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
                || (keyCode == KeyEvent.KEYCODE_CAMERA)) {
            return true;
        }
        // Key code constant: Home key. This key is handled by the framework and is never delivered to applications.
        // so I can't just override this method to handle home button key event
        return keyCode == KeyEvent.KEYCODE_HOME;
    }

    // clear focus on touch outside EdiText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            unlockScreen();
            View view = getCurrentFocus();
            Rect outRect = new Rect();
            if (view != null) {
                view.getGlobalVisibleRect(outRect);
            }
            if (view instanceof EditText) { // clear focus and hide keyboard if touch outside EditText
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    view.clearFocus();
                    hideSoftKeyboard();
                }
            } else { // show keyboard for EditText
                mEditTextPassphrase.getGlobalVisibleRect(outRect);
                if (outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    showSoftKeyboard();
                }
            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            lockScreen();
        }
        return super.dispatchTouchEvent(event);
    }

    private void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditTextPassphrase.getWindowToken(), 0);
    }

    private void showSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEditTextPassphrase, InputMethodManager.SHOW_IMPLICIT);
    }

    // There is no way to hide navigation buttons permanently for activity without special permissions
    private void hideNavigation() {
        if (Build.VERSION.SDK_INT >= 19) {
            final View decorView = getWindow().getDecorView();
            final int flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(flags);
            decorView.setOnSystemUiVisibilityChangeListener(
                    new View.OnSystemUiVisibilityChangeListener() {
                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }
    }

    // Handle events of calls and unlock screen if necessary
    private class StateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    unlockScreenAndFinish();
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    break;
            }
        }
    }

}
