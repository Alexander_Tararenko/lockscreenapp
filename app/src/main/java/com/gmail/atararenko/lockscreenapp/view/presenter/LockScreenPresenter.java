package com.gmail.atararenko.lockscreenapp.view.presenter;

import android.app.WallpaperManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;

import com.gmail.atararenko.lockscreenapp.LockScreenApplication;
import com.gmail.atararenko.lockscreenapp.R;
import com.gmail.atararenko.lockscreenapp.view.ILockScreenView;

import java.io.IOException;

public class LockScreenPresenter implements ILockScreenPresenter {

    private ILockScreenView mView;
    private Context mContext;
    private String pass;

    public LockScreenPresenter(ILockScreenView mView, Context mContext) {
        this.mView = mView;
        this.mContext = mContext;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        pass = preferences.getString(LockScreenApplication.SHARED_PREF_PASS, "hello");
        setGreetingsText();
        initSystemWallpaper();
    }

    @Override
    public void validatePassphrase() {
        if (mView.getEnteredPass().equalsIgnoreCase(pass)) {
            mView.unlockScreenAndFinish();
        }
    }

    private void setGreetingsText() {
        final String[] projection = new String[]
                {ContactsContract.Profile.DISPLAY_NAME};
        String name = null;
        final Uri dataUri = Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY);
        final ContentResolver contentResolver = mContext.getContentResolver();
        final Cursor c = contentResolver.query(dataUri, projection, null, null, null);
        if (c != null && c.moveToFirst()) {
            name = c.getString(c.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME));
        }
        if (c != null) {
            c.close();
        }
        StringBuilder greetingsText = new StringBuilder();
        greetingsText.append(mContext.getString(R.string.greetings));
        if (name != null && !name.isEmpty()) {
            greetingsText.append(", ");
            greetingsText.append(name);
        }
        greetingsText.append("!");
        mView.setGreetingText(greetingsText.toString());
    }

    private void initSystemWallpaper() {
        WallpaperManager manager = WallpaperManager.getInstance(mContext);
        Drawable backgroundImage;
        if (manager.getWallpaperInfo() == null) {
            backgroundImage = manager.getDrawable();
        } else {
            // user likes live wallpapers
            // set default wallpaper instead
            try {
                backgroundImage = Drawable.createFromStream(mContext.getAssets().open("android-n-stock-wallpaper.jpg"), null);
            } catch (IOException e) {
                e.printStackTrace();
                backgroundImage = manager.getDrawable();
            }
        }
        mView.setWallpaper(backgroundImage);
    }

}
