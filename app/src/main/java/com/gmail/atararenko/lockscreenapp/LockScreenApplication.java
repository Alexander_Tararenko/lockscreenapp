package com.gmail.atararenko.lockscreenapp;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LockScreenApplication extends Application {

    public static final String SHARED_PREF_PASS = "pass";

    @Override
    public void onCreate() {
        super.onCreate();
        initPass();
    }

    private void initPass() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String pass = preferences.getString(SHARED_PREF_PASS, "");
        if (pass.isEmpty()) {
            preferences.edit()
                    .putString(SHARED_PREF_PASS, "hello") // default value
                    .apply();
        }
    }

}