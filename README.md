# README #

### What is this repository for? ###

Task for Android Developer in [BelHard.com](http://belhard.com/en/)

### Coding task ###

Please create simple application on Android using Android SDK. You can create normal app or cordova plugin.

- Create lock screen.
- it must contain image, text, and input
- If you type word "Hello" it must unlock the phone

### TODO ###

- Doesn't work on Android 6.0+. It requires android.permission.INTERNAL_SYSTEM_WINDOW which is a signature-level permission.

- Sometimes doesn't work on devices with on-screen navigation bar

- Request runtime permissions for Android 6.0+

- Better to disable system lock screen

### Screenshots ###

![Screenshot_2016-04-25-19-37-42.png](https://bitbucket.org/repo/dkn98M/images/575652526-Screenshot_2016-04-25-19-37-42.png) ![Screenshot_2016-04-25-19-38-10.png](https://bitbucket.org/repo/dkn98M/images/2996250650-Screenshot_2016-04-25-19-38-10.png)